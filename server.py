#!../bin/python
# -*- coding: utf-8 -*-
from app.flaskapp import app


def main():
    import sys
    if len(sys.argv) == 2:
        app.config['DATADIR'] = sys.argv[1]

    app.run(
        debug=True,
    )


if __name__ == '__main__':
    main()
