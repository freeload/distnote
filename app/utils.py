# -*- coding: utf-8 -*-
import datetime
import logging
from itertools import chain

log = logging.getLogger(__name__)

def nowstamp():
    return datetime.datetime.utcnow().isoformat() + "Z"

def dumptable(db, tablename):
    return query_db(db, 'SELECT * FROM "%s" LIMIT 1000' % tablename)


def _dbg_query(query, args):
    d = query.split('?')
    e = (repr(unicode(arg)) for arg in args)
    print d[0] + "".join(chain(*zip(e, d[1:])))


def query_db(db, query, args=(), one=False):
    _dbg_query(query, args)

    cur = db.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
               for idx, value in enumerate(row)) for row in cur.fetchall()]
    return (rv[0] if rv else None) if one else rv
