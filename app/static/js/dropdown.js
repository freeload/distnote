$(function(){
    $('#topnav > ul > li > ul').parent().each(function(){
        var lang = $(this);
        var choice = $('ul', lang);

        lang.hover(function(){
            var pos = lang.offset();
            var choice_left = pos.left + lang.width() - choice.width() - 2;
            var choice_top = pos.top + lang.height();
            choice.slideDown('fast');
            choice.offset({'top': choice_top, 'left': choice_left});
        }, function(){
                choice.hide();
        });
    });

    // Make the categories menu sticky
    var categories = $("#categories").first();
    var offset_y = categories.offset().top;
    $(window).scroll(function(event){
        if (offset_y - $(this).scrollTop() < 0) {
            categories.addClass("sticky");
        } else {
            categories.removeClass("sticky");
        }
    });
});
