#!../bin/python
# -*- coding: utf-8 -*-
import jinja2
from flask import (
    Flask,
    g,
    jsonify,
    redirect,
    render_template,
    request,
    Response,
    url_for,
)
app = Flask(__name__)
app.jinja_env.undefined = jinja2.StrictUndefined
app.config['DATASUBDIR'] = 'default'

import app.model.document as document
import app.sync as sync
from app.db import notedb
from app.utils import dumptable
from subprocess import check_output
from werkzeug.exceptions import NotFound



@app.errorhandler(404)
def error404(e):
    return render_template('404.html'), 404


def debug(*objects):
    return render_template('debug.html', objects)


@app.route('/')
def latest():
    notes = document.latest(notedb())
    return render_template('latest.html', notes=notes)


@app.route('/create', methods=['GET'])
def create():
    return render_template('edit.html')


@app.route('/create', methods=['POST'])
def create_do():
    content = request.form.get(u'content')
    document.create(notedb(), content)
    return redirect(url_for('latest'))


@app.route('/note/<string:noteid>', methods=['GET'])
def edit(noteid):
    note = document.get(notedb(), noteid)
    if note is None:
        raise NotFound()
    return render_template('edit.html',
       content=note['content'],
       id = note['id']
    )


@app.route('/note/<string:noteid>', methods=['POST'])
def edit_do(noteid):
    content = request.form.get('content')
    document.update(notedb(), noteid, content)
    return redirect(url_for('latest'))


@app.route('/note/<string:noteid>/delete', methods=['GET'])
def delete(noteid):
    document.delete(notedb(), noteid)
    return redirect(url_for('latest'))


@app.route('/admin')
def admin():
    return render_template('admin.html')


import requests
@app.route('/admin/connect', methods=['POST'])
def connect():
    state = sync.localstate(notedb())
    return repr(state)
    request.get('http://127.0.0.1:5000/sync', params=state)
    


@app.route('/syncdata', methods=['GET'])
def sync_get():
    state = request.args.items(multi=True)
    state = sync.getnew(notedb(),state)
    return jsonify({"documents": state})


@app.route('/syncdata', methods=['PUT'])
def sync_put():
    data = request.json
    for doc in data['documents']:
        sync.mergedoc(notedb(), doc)
    return '', 204


@app.route('/status')
def status():
    return render_template('status.html',
        vars = dumptable(notedb(), 'var'),
        documents = dumptable(notedb(), 'document'),
        sync_log = dumptable(notedb(), 'sync_log')
    )


@app.route('/style')
def style():
    response = Response()
    response.data = check_output(['lessc', 'less/style.less'])
    response.headers['Cache-Control'] = 'max-age=3600, must-revalidate'
    response.mimetype = 'text/css'
    return response
