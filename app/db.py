# -*- coding: utf-8 -*-
import os
import os.path as path
import sqlite3
from app.flaskapp import app

notedb_initsql = """
    CREATE TABLE "var" (
        "key" TEXT,
        "value" TEXT
    );
    CREATE UNIQUE INDEX "idx_key" ON "var" ("key" ASC);
    
    CREATE TABLE "sync_log"(
        "date" TEXT,
        "ip" TEXT,
        "node_initiator" TEXT,
        "node_target" TEXT,
        "seq_id" INTEGER
    );

    CREATE TABLE "document"(
        "id" TEXT,
        "node_id" TEXT,
        "seq_id" INTEGER,
        "mtime" TEXT, 
        "content" TEXT,
        "deleted" INTEGER
    );
    CREATE UNIQUE INDEX "idx_id"          ON "document" ("id" ASC);
    CREATE UNIQUE INDEX "idx_replication" ON "document" ("node_id" ASC, "seq_id" ASC);
    CREATE INDEX        "idx_latest"      ON "document" ("deleted" ASC, "mtime" DESC);
"""
def preparedatadir():
    dir_ = "data/" + app.config['DATASUBDIR']
    if not path.isdir(app.config['da']):
        os.mkdir("data")

def generatetestdata(db):
    import app.model.document as document
    print "Creating 200 test docs..."
    for i in range(200):
        document.create(db, "Testdoc " + str(i + 1))
    print "Done"

def notedb_lazy():
    db = [None]
    def getter():
        if db[0] is None:
            preparedatadir()
            new = not path.exists("data/notes")
            db[0] = sqlite3.connect("data/notes", check_same_thread=False)
            db[0].execute("PRAGMA synchronous=OFF")
            db[0].execute("PRAGMA journal_mode=OFF")
            db[0].isolation_level = None
            if new:
                db[0].executescript(notedb_initsql)
                generatetestdata(db[0])
        return db[0]
    return getter

notedb = notedb_lazy()
