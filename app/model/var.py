# -*- coding: utf-8 -*-
import logging
log = logging.getLogger(__name__)

def getvar(db, key):
    sql = 'SELECT "value" FROM "var" WHERE "key" = ?'
    for value, in db.execute(sql, (key,)):
        return value


def setvar(db, key, value):
    sql = 'UPDATE "var" SET "value" = ? WHERE "key" = ?'
    if db.execute(sql, (value,key)).rowcount <= 0:
        sql = 'INSERT INTO "var"("key","value") VALUES(?,?)'
        db.execute(sql, (key,value))
        log.info("Var %s set to %s", repr(key), repr(value))


def popseqid(db):
    db.execute('BEGIN EXCLUSIVE')
    seqid = getvar(db, "last_seqid")
    if seqid is None:
        seqid = 1
    else:
        seqid = int(seqid) + 1
    setvar(db, "last_seqid", unicode(seqid))
    db.execute('END')

    return seqid


