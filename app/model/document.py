# -*- coding: utf-8 -*-
import datetime
import uuid
import re
import logging

import app.model.var as var
from app.utils import nowstamp, query_db

log = logging.getLogger(__name__)

def getpeerid(db):
    id_ = var.getvar(db, "node_id")
    
    if id_ is not None:
        return id_
    
    id_ = unicode(uuid.uuid4())

    var.setvar(db, "node_id", id_)
    return id_
        
def create(db, content):
    id_ = unicode(uuid.uuid4())
    mtime = nowstamp()
    peerid = getpeerid(db)
    seqid = var.popseqid(db)
    
    query_db(db,
        'INSERT INTO "document" ('
            '"id", "node_id", "seq_id", "mtime", "content", "deleted"'
        ') VALUES ('
            '?, ?, ?, ?, ?, 0'
        ')'
    , (id_, peerid, seqid, mtime, content))

    return id_, mtime

def update(db, id_, content):
    mtime = nowstamp()
    peerid = getpeerid(db)
    seqid = var.popseqid(db)
    
    sql = (
        'UPDATE "document" SET '
            '"node_id" = ?,'
            '"seq_id" = ?,'
            '"mtime" = ?,'
            '"content" = ?'
        'WHERE "id" = ?'
    )
    if db.execute(sql, (peerid, seqid, mtime, content, id_)).rowcount < 1:
        return

    return mtime

def get(db, id_):
    return query_db(db,
        'SELECT * FROM "document" '
        'WHERE "id" = ? AND "deleted" = 0'
        , (id_,), one=True)

def latest(db):
    return query_db(db,
        u'SELECT * FROM "document" '
        u'WHERE "deleted" = 0 '
        u'ORDER by mtime desc LIMIT 8'
    )

def delete(db, id_):
    mtime = nowstamp()
    peerid = getpeerid(db)
    seqid = var.popseqid(db)
    
    sql = '''
        UPDATE "document" SET
            "node_id" = ?,
            "seq_id" = ?,
            "mtime" = ?,
            "content" = NULL,
            "deleted" = 1
        WHERE "id" = ?
    '''
    if db.execute(sql, (peerid, seqid, mtime, id_)).rowcount > 0:
        return mtime,
