# -*- coding: utf-8 -*-
from app.model.var import getvar
from app.utils import query_db, nowstamp
from itertools import chain


def localstate(db):
    return dict((row['node_id'],row['seq_id']) for row in query_db(db,
        'SELECT "node_id", max("seq_id") AS "seq_id" '
        'FROM "document" '
        'GROUP BY "node_id"'
    ))
    

def getnew(db, state):
    query = 'SELECT "id", "mtime" FROM "document"'
    if len(state) > 0:
        query += ' WHERE ' + ' OR '.join(['("node_id" = ? AND "seq_id" > ?)'] * len(state))

    args = tuple(chain(*state))

    return query_db(db, query, args)


def mergedoc(db, doc):
    local = query_db(db, 'SELECT * FROM "document" WHERE id = ?', (doc['id'],), one=True)
    
    if local is None:
        query_db(db,
            'INSERT INTO "document" ('
                '"id", "node_id", "seq_id", "mtime", "content", "deleted"'
            ') VALUES ('
                '?, ?, ?, ?, ?, ?'
            ')'
            , (doc['id'], doc['node_id'], doc['seq_id'], doc['mtime'], doc['content'], doc['deleted'])
        )
        return
    
    if local['node_id'] == doc['node_id'] and local['seq_id'] == doc['seq_id']:
        return

    if local['mtime'] >= doc['mtime']:
        return

    query_db(db,
        'UPDATE "document" SET'
            ' "node_id" = ?'
            ', "seq_id" = ?'
            ', "mtime" = ?'
            ', "content" = ?'
            ', "deleted" = ?'
            ' WHERE "id" = ?'
        , (doc['node_id'], doc['seq_id'], doc['mtime'], doc['content'], doc['deleted'], doc['id'])
    )
